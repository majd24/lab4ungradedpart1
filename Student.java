public class Student {
	
	private String name;
	private String ID;
	private double grade;
	
	public int amountLearnt;
	
	public void sayName() {
		
		System.out.println("My name is " + name);
		
	}
	
	public void sayID() {
		
		System.out.println("My ID is " + ID);
		
	}
	
	public void learn(int amountStudied) {
		if(amountStudied > 0) {
			amountLearnt+=amountStudied;
		}
	}
	
	
	public void setName(String newName) {
		this.name = newName;
		
	}
	
	public String getName() {
		return this.name;
		
	}
	
	public void setID(String newID) {
		this.ID = newID;
		
	}
	
	public String getID() {
		
		return this.ID;
		
		
	}
	
	public void setGrade(double newGrade) {
		this.grade = newGrade;
		
	}
	
	
	public double getGrade() {
		return this.grade;
		
	}
	
}